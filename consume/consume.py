import requests

STATUS_OK = 200

# Consuming a REST/API endpoint 
resp_data = requests.get(url="https://dummyjson.com/products/1")
print(f"The type of resp_data: {type(resp_data)}")

# Get the response data
status_code = resp_data.status_code
print(f"The status code of the GET method is {status_code}")

# Status codes
# 200 : OK (Success)
# 201 : Created 
# 202 : Accepted

# 301 : Moved Permanently 
# 304 : NOt modified
# 
# 400 : Bad Request
# 401 : Unauthorized
# 403 : Forbidden
# 404 : NOt Found
# 
# 500 : Internal Server Error
# 503 : Service Unavailable
# 

if status_code == STATUS_OK:
    content = resp_data.content
    print(f"The content is {content}")

    prod_data = resp_data.json()
    print()
    print(f"The json data prod_data is {prod_data}")
    print(f"The id of this data is {prod_data['id']}")
    print(f"The description of this data is {prod_data['description']}")
